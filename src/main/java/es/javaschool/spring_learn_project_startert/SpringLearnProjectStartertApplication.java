package es.javaschool.spring_learn_project_startert;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringLearnProjectStartertApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringLearnProjectStartertApplication.class, args);
	}

}
